**DEPRECATED in favor of Netlify.. Can't beat their service..**


--

Home Web Project configuration on a Virtual Private Server 😎
=============================================================

## Connect to the server and run this project
Once the VPS server is running, connect to the server using ssh: 
**Install dependencies**
* Install git: `sudo yum -y install git`
* Install docker-compose: `sudo yum -y install epel-release && sudo yum install -y python-pip && sudo pip install docker-compose`

## Generate the ssh keys
* Run `ssh-keygen -t rsa -b 4096 -C "your_email@example.com"` and press enter the whole way 🛣
* Ensure the ssh-agent is running: `eval $(ssh-agent -s)`
```
Agent pid 1657
```
* Add the SSH private key to your agent: `ssh-add ~/.ssh/id_rsa`
* Copy the ssh key `cat ~/.ssh/id_rsa.pub`
* Paste it under Gitlab to the **SSH Keys** under _settings_

## Clone this project
* Run: `mkdir webprojects && cd webprojects && git clone git@gitlab.com:thomasmorice/reverse-proxy-ci.git`
* Rename .env.example `mv .env.example .env`
* Change permission to file _acme.json_ `chmod 600 acme.json`

## Add info to .env
* Add DroneCI to gitlab by Going to Settings > Applications 
* The redirection URI should be: `http://server-ip:8000/authorize`
* Give **api** as the scope to be able to have full access
* Press **save application** & get the information needed for the `.env` file (`DRONE_GITLAB_CLIENT` & `DRONE_GITLAB_SECRET`)
* Remove `COMPOSE_CONVERT_WINDOWS_PATHS=1` since we are not in windows 😊

## Run the project using docker-compose and configure drone
* Run `cd reverse-proxy-ci && docker-compose up -d`
* Connect from the browser to **http://server-ip:8000** and authorize the API 
* Activate the repository you want to use, a link on the right of the repo will appear
* Open it, and go to the settings of the repository to make it **trusted** 
* Add **secrets** variables you need in order for `.drone.yml` to run successfully (drone/ssh stuff)

Your project should now build successfully on next push 🎈

_of course do not forget to point the **DNS** Type **a** on cloudflare or whatever is your Domain Name service 🤷‍_